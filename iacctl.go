package main

import (
	"fmt"
	"os"
	"io/ioutil"

	"gopkg.in/yaml.v2"
	"actency.fr/iacctl/utils"
)

// Function that check that the error is empty, otherwise triggers panic method
func check(e error) {
	if e != nil {
		panic(e)
	}
}

// YAML Structure
type Values struct {
	Namespace string `yaml:"namespace"`
	Environment string `yaml:"environment"`
	
	Registry struct {
		Name string `yaml:"name"`
		Location string `yaml:"location"`

		Image struct {
			Drupal string `yaml:"drupal"` 
			ContentHub string `yaml:"contenthub"` 
			Varnish string `yaml:"varnish"` 
		} `yaml:"images"`
	} `yaml:"registry"`

	Toto []string `yaml:"toto"` 
}

// Check args provided when calling the iacctl
func checkInput () {
	if (len(os.Args) < 3) {
		fmt.Fprintln(os.Stderr, "Usage : iaac <source_file> <target_file>")
		os.Exit(3)
	}
}

// Main
func main () {
	checkInput()

	// Read values.yaml file
	dat, err := ioutil.ReadFile("values.yaml")
	check(err)
	fmt.Print(string(dat))

	// Instanciate a Values structure
	values := Values{}

	// Parse the YAML file and fill the values structure
	err = yaml.Unmarshal([]byte(dat), &values)
	check(err)

	// Some dumping examples
	fmt.Printf("--- values dump: \n%v\n\n", values)
	fmt.Printf("--- values contenthub : \n%s\n\n", values.Registry.Image.ContentHub)
	fmt.Printf("--- values tata : \n%s\n\n", values.Toto[1])
	fmt.Printf("--- values tata : \n%d\n\n", len(values.Toto))

	// Replace in templates/example all REPLACE words by iacctl
	err = utils.ReplaceTextInFile("templates/example", "REPLACE", "iacctl")
	check(err)
	
	// Copy a file example. Same than cp <source> <dest> but in go
	fmt.Printf("Copying %s to %s\n", os.Args[1], os.Args[2])
    err = utils.CopyFile(os.Args[1], os.Args[2])
    if err != nil {
        fmt.Printf("CopyFile failed %q\n", err)
    } else {
        fmt.Printf("CopyFile succeeded\n")
    }
}