module actency.fr/iacctl

go 1.15

require gopkg.in/yaml.v2 v2.3.0
replace actency.fr/iactl/ => ./
