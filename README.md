Compile the client:
```sh
go build iacctl.go
```

Run the client after compilation
```sh
./iacctl <source> <target>
```
